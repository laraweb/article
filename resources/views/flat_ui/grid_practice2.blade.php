@extends('layouts.flat_ui')
@section('title', 'Grid System｜Flat UI')
@section('content')
        <div class="row">
            <div class="col-sm-3 col-xs-6 bg-success">.col-sm-3 .col-xs-6</div>
            <div class="col-sm-3 col-xs-6 bg-info">.col-sm-3 .col-xs-6</div>
            <div class="col-sm-3 col-xs-6 bg-warning">.col-sm-3 .col-xs-6</div>
            <div class="col-sm-3 col-xs-6 bg-danger">.col-sm-3 .col-xs-6</div>
        </div>
@endsection