@extends('layouts.flat_ui')
@section('title', 'Grid System｜Flat UI')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 bg-success">col-sm-8</div>
            <div class="col-sm-4 bg-info">col-sm-4</div>
        </div>
    </div>
@endsection