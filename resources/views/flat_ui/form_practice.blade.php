@extends('layouts.flat_ui')
@section('title', 'Form example｜Flat UI')
@section('content')
<form class="form-horizontal" style="margin-top: 50px;">

        <div class="form-group">
            <label class="col-sm-2 control-label" for="InputName">氏名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="InputName" placeholder="氏名">
                <!--/.col-sm-10---></div>
            <!--/form-group--></div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="InputEmail">メール・アドレス</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="InputEmail" placeholder="メール・アドレス">
            </div>
            <!--/form-group--></div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="InputPassword">パスワード</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="InputPassword" placeholder="パスワード">
            </div>
            <!--/form-group--></div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="InputSelect">選択</label>
            <div class="col-sm-10">
                <select data-toggle="select" class="form-control select select-default" id="InputSelect">
                    <option>選択肢１</option>
                    <option>選択肢２</option>
                    <option>選択肢３</option>
                </select>
            </div>
            <!--/form-group--></div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="form-group">
                    <div class="checkbox-inline">
                    <label class="checkbox" for="checkbox5a">
                        <input type="checkbox" data-toggle="checkbox" value="" id="checkbox5a" required checked> 1
                    </label>
                    </div>
                    <div class="checkbox-inline">
                    <label class="checkbox" for="checkbox5b">
                        <input type="checkbox" data-toggle="checkbox" value="" id="checkbox5b" required > 2
                    </label>
                    </div>
                    <div class="checkbox-inline">
                    <label class="checkbox" for="checkbox5c">
                        <input type="checkbox" data-toggle="checkbox" value="" id="checkbox5c" required > 3
                    </label>
                    </div>
                </div>
            </div>
        <!--/form-group--></div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-block">送信</button>
            </div>
        <!--/form-group--></div>

</form>
@endsection