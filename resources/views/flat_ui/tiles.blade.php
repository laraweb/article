@extends('layouts.flat_ui')
@section('title', 'Tiles｜Flat UI')
@section('content')

    <div class="container">
        <h4>Tiles</h4>
        <div class="row">
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="tile">
                            <img src="/img/icons/svg/compas.svg" alt="Compas" class="tile-image big-illustration">
                            <h3 class="tile-title">Web Oriented</h3>
                            <p>100% convertable to HTML/CSS layout.</p>
                            <a class="btn btn-primary btn-large btn-block" href="http://designmodo.com/flat">Get Pro</a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="tile">
                            <img src="/img/icons/svg/loop.svg" alt="Infinity-Loop" class="tile-image">
                            <h3 class="tile-title">Easy to Customize</h3>
                            <p>Vector-based shapes and minimum of layer styles.</p>
                            <a class="btn btn-primary btn-large btn-block" href="http://designmodo.com/flat">Get Pro</a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="tile">
                            <img src="/img/icons/svg/pencils.svg" alt="Pensils" class="tile-image">
                            <h3 class="tile-title">Color Swatches</h3>
                            <p>Easy to add or change elements. </p>
                            <a class="btn btn-primary btn-large btn-block" href="http://designmodo.com/flat">Get Pro</a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="tile">
                            <img src="/img/icons/svg/ribbon.svg" alt="ribbon" class="tile-hot-ribbon">
                            <img src="/img/icons/svg/chat.svg" alt="Chat" class="tile-image">
                            <h3 class="tile-title">Free for Share</h3>
                            <p>Your likes, shares and comments helps us.</p>
                            <a class="btn btn-primary btn-large btn-block" href="http://designmodo.com/flat">Get Pro</a>
                        </div>

                    </div>
                </div> <!-- /row -->
            </div>
        </div> <!-- /row -->
    </div><!-- /.container -->
@endsection
