<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>sample</title>
    <script src="https://unpkg.com/vue"></script>
</head>
<body>
<div id="app">
    <p>@{{ message }}</p>
    <button v-on:click="messageChange('PHPのフレームワークといえばLaravel！')">「PHP」が好き</button>
    <button v-on:click="messageChange('RubyのフレームワークといえばRails！')">「Ruby」が好き</button>
    <button v-on:click="messageChange('JavaScriptのライブラリといえばVue.js！')">「JavaScript」が好き</button>
</div>
<script>
    var app = new Vue({
    el: '#app',
        data: {
        message: '好きな言語は？'
        },
        methods:{
        messageChange: function (lang){
            this.message=lang;
        }
    }
    })
</script>
</body>
</html>
