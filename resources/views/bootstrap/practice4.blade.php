<!doctype html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>test</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/css/sticky-footer.css" rel="stylesheet" media="screen">
</head>
<body>

<div class="container">
    <!-- navbar -->
    <div class="row">
        <nav class="navbar navbar-default navbar-fixed-top">

            <div class="navbar-header">
                <a href="" class="navbar-brand">Home</a>
            </div>

            <ul class="nav navbar-nav">
                <li class="active"><a href="">menu1</a></li>
                <li><a href="">menu2</a></li>
                <li><a href="">menu3</a></li>
                <li><a href="">menu4</a></li>
            </ul>

        </nav>
    </div>

    <!-- content -->
    <div class="row" id="content" style="padding:80px 0 0 0">
                <!-- left -->
                <div class="col-md-3">
                    <!-- パネルで囲む -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Menu
                        </div>
                        <!-- <div class="panel-body"> -->
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href=""><i class="glyphicon glyphicon-menu-right"></i> submenu1</a></li>
                            <li><a href=""><i class="glyphicon glyphicon-menu-right"></i> submenu2</a></li>
                            <li><a href=""><i class="glyphicon glyphicon-menu-right"></i> submenu3</a></li>
                            <li><a href=""><i class="glyphicon glyphicon-menu-right"></i> submenu4</a></li>
                        </ul>
                        <!-- </div> -->
                    </div>
                </div>

        <!-- main -->
        <div class="col-md-9">
            <!-- apply custom style -->
            <div class="page-header" style="margin-top:-30px;padding-bottom:0px;">
                <h1><small>タイトル</small></h1>
            </div>

            <div class="row">

                <!-- この単位を繰り返す -->
                <div class="col-md-4">
                    <div class="thumbnail" style="text-align:center;padding-top:10px;">
                        <a href="">
                            <img src="{{\Config::get('fpath.noimage')}}">
                        </a>
                        <div class="caption">
                            <p><b>Caption</b></p>
                            <p>this is a caption.</p>
                            <p style="margin-bottom:0px"><a href="" class="btn btn-primary">to cart</a></p>
                        </div>
                    </div>
                </div>
                <!-- ここまで -->

                <!-- この単位を繰り返す -->
                <div class="col-md-4">
                    <div class="thumbnail" style="text-align:center;padding-top:10px;">
                        <a href="">
                            <img src="{{\Config::get('fpath.noimage')}}">
                        </a>
                        <div class="caption">
                            <p><b>Caption</b></p>
                            <p>this is a caption.</p>
                            <p style="margin-bottom:0px"><a href="" class="btn btn-primary">to cart</a></p>
                        </div>
                    </div>
                </div>
                <!-- ここまで -->

                <!-- この単位を繰り返す -->
                <div class="col-md-4">
                    <div class="thumbnail" style="text-align:center;padding-top:10px;">
                        <a href="">
                            <img src="{{\Config::get('fpath.noimage')}}">
                        </a>
                        <div class="caption">
                            <p><b>Caption</b></p>
                            <p>this is a caption.</p>
                            <p style="margin-bottom:0px"><a href="" class="btn btn-primary">to cart</a></p>
                        </div>
                    </div>
                </div>
                <!-- ここまで -->

                <!-- この単位を繰り返す -->
                <div class="col-md-4">
                    <div class="thumbnail" style="text-align:center;padding-top:10px;">
                        <a href="">
                            <img src="{{\Config::get('fpath.noimage')}}">
                        </a>
                        <div class="caption">
                            <p><b>Caption</b></p>
                            <p>this is a caption.</p>
                            <p style="margin-bottom:0px"><a href="" class="btn btn-primary">to cart</a></p>
                        </div>
                    </div>
                </div>
                <!-- ここまで -->

                <!-- この単位を繰り返す -->
                <div class="col-md-4">
                    <div class="thumbnail" style="text-align:center;padding-top:10px;">
                        <a href="">
                            <img src="{{\Config::get('fpath.noimage')}}">
                        </a>
                        <div class="caption">
                            <p><b>Caption</b></p>
                            <p>this is a caption.</p>
                            <p style="margin-bottom:0px"><a href="" class="btn btn-primary">to cart</a></p>
                        </div>
                    </div>
                </div>
                <!-- ここまで -->

                <!-- この単位を繰り返す -->
                <div class="col-md-4">
                    <div class="thumbnail" style="text-align:center;padding-top:10px;">
                        <a href="">
                            <img src="{{\Config::get('fpath.noimage')}}">
                        </a>
                        <div class="caption">
                            <p><b>Caption</b></p>
                            <p>this is a caption.</p>
                            <p style="margin-bottom:0px"><a href="" class="btn btn-primary">to cart</a></p>
                        </div>
                    </div>
                </div>
                <!-- ここまで -->

            </div><!-- end row -->
        </div><!-- end main -->

    </div><!-- end content -->

</div>

<!-- footer -->
<footer class="footer">
    <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>