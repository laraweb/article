@extends('layouts.master_employee')
@section('title', '従業員リスト')

@section('table')
    <table class="table table-striped">
        <tr>
            <th>部署</th>
            <th>従業員名</th>
            <th>住所</th>
            <th>メールアドレス</th>
            <th>年齢</th>
            <th>電話番号</th>
        </tr>
    <!-- loop -->
    @foreach($employees as $employee)
        <tr>
            <td>{{$employee->dept_name}}</td>
            <td>{{$employee->name}}</td>
            <td>{{$employee->address}}</td>
            <td>{{$employee->email}}</td>
            <td>{{$employee->old}}</td>
            <td>{{$employee->tel}}</td>
        </tr>
    @endforeach
</table>

    <!-- pager -->
    <div class="paginate text-center">
        {!! $employees->render() !!}
    </div>

@endsection


