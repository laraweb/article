@extends('layouts.master_special')
@section('title', 'Authミドルウェア｜特設ページ')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">トップ</div>

                <div class="panel-body">
                    Laravel5.2<br>
                    認証デモンストレーション<br>
                    特設ページは<a href="{{ url('/special/01') }}">こちら</a>です。
                </div>
            </div>
        </div>
    </div>
</div>

@endsection