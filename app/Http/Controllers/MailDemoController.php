<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;

class MailDemoController extends Controller
{
    private $to_address = "laravel@honjou.com";

    public function getIndex()
    {
        return view('mail.index');
    }

    public function confirm(\App\Http\Requests\MailDemoRequest $request)
    {
        $data = $request->all();
        return view('mail.confirm')->with($data);
    }

    public function finish(\App\Http\Requests\MailDemoRequest $request)
    {
        $data = $request->all();
        Mail::send('mail.temp', $data, function($message) use($data){
            $message->to($data["email"])->subject($data["title"]);
        });

        // for admin
        Mail::send('mail.temp', $data, function($message) use($data){
            $message->to($this->to_address)->subject($data["title"]);
        });
        return view('mail.finish');
    }
}
