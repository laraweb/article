<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ComponentsController extends Controller
{
    // for Flat UI
    public function parts($name){
        return view('flat_ui.'.$name);
    }

    // for Bootstrap
    public function practice($name){
        return view('bootstrap.'.$name);
    }

    // for Tutorial
    public function user_list($name){
        return view('users.'.$name);
    }

}
