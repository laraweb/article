<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UploaderController extends Controller
{
    public function getIndex()
    {
        $uploaders = \App\Uploader::orderBy('created_at', 'desc')->paginate(5);

        $hash = array(
            'uploaders' => $uploaders,
        );

        return view('uploader.index')->with($hash);
    }

    public function confirm(\App\Http\Requests\UploaderRequest $req)
    {
        $username = $req->username;
        $thum_name = uniqid("THUM_") . "." . $req->file('thum')->guessExtension(); // TMPファイル名
        $req->file('thum')->move(public_path() . \Config::get('fpath.tmp'), $thum_name);
        $thum = \Config::get('fpath.tmp').$thum_name;

        $hash = array(
            'thum' => $thum,
            'username' => $username,
        );

        return view('uploader.confirm')->with($hash);
    }

    public function finish(Request $req)
    {

        $uploader = new \App\Uploader;
        $uploader->username = $req->username;
        $uploader->save();

        // get insert id from an eloquent model
        $lastInsertedId = $uploader->id;

        // make a directory and move uploaded image to it
        if (!file_exists(public_path() .\Config::get('fpath.thum') . $lastInsertedId)) {
            mkdir(public_path() . \Config::get('fpath.thum') . $lastInsertedId, 0777);
        }

        // rename
        rename(public_path() . $req->thum, public_path() . \Config::get('fpath.thum') . $lastInsertedId . "/thum." .pathinfo($req->thum, PATHINFO_EXTENSION));

        return view('uploader.finish');
    }
}
