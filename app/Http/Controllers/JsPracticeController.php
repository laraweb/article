<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class JsPracticeController extends Controller
{
    public function vue()
    {
        return view('js.vue_example');
    }
}
