<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Employee;

use Log;

class EmployeeController extends Controller
{
    //一覧
    public function select(Request $req){

        // 値を取得
        $dept_name = $req->input('dept_name');
        $pref = $req->input('pref');

        // DEBUG
        Log::debug('$dept_name="'.$dept_name.'"');
        Log::debug('$pref="'.$pref.'"');

        // 検索QUERY
        $query = Employee::query();

        // 結合
        $query->join('depts', function ($query) use ($req) {
            $query->on('employees.dept_id', '=', 'depts.id');
        });

        // もし「部署名」があれば
        if(!empty($dept_name)){
            $query->where('dept_name','like','%'.$dept_name.'%');
        }

        // もし「都道府県」があれば
        if(!empty($pref)){
            $query->where('address','like','%'.$pref.'%');
        }

        // ページネーション
        $employees = $query->paginate(5);

        // DEBUG
        $employees_sql = $query->toSql();
        Log::debug('$employees_sql="'.$employees_sql.'""');

        // ビューへ渡す値を配列に格納
        $hash = array(
            'dept_name' => $dept_name, //pass parameter to pager
            'pref' => $pref, //pass parameter to pager
            'employees' => $employees,   //Eloquent
        );

        return view('employee.list')->with($hash);
    }

    public function scopeDemo()
    {
        // 検索QUERY
        $query = Employee::query();

        //結合
        $query->join('depts', function ($query) {
            $query->on('employees.dept_id', '=', 'depts.id');
        });

        //条件式
        $query->FindDevelop()->FindKansai();
//        $query->where('dept_id','4');
//        $query->where(function($q){
//            $q->Where('address','like','%'.'大阪府'.'%')
//                ->orWhere('address','like','%'.'京都府'.'%')
//                ->orWhere('address','like','%'.'兵庫県'.'%')
//                ->orWhere('address','like','%'.'滋賀県'.'%')
//                ->orWhere('address','like','%'.'奈良県'.'%')
//                ->orWhere('address','like','%'.'和歌山県'.'%');
//        });

        // ページネーション
        $employees = $query->paginate(10);

        //debug
        $employees_sql = $query->toSql();
        Log::debug('$employees_sql="'.$employees_sql.'""');


        //test
//        $query->paginate(10);
//        $employees =$query->toSql();
//        dd($employees);

        return view('employee.scope_demo')->with('employees',$employees);

    }

    //一覧
    public function judge()
    {
        return view('demo.judge_device');
    }

    }
