<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SpecialController extends Controller
{
   /*
    * 認証
    */ 
    public function __construct() {
        $this->middleware('auth');
    }

    public function page01() {
        return view('special.page01');
    }
    
    public function page02() {
        return view('special.page02');
    }
    
    public function page03() {
        return view('special.page03');
    }
    
}
