<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CountController extends Controller
{
    //countdown tokyo olympic
    /**
     * @return $this
     */
    function olympic(){
        $end_day = strtotime( '20200724' ) ;
        $int = $end_day - time();
        $after_day = ceil($int / (24 * 60 * 60));
        return view('count.olympic')->with('after_day',$after_day);
    }
}
