<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 * Laravel初期設定画面
 */
Route::get('/', function () {
    return view('welcome');
});


Route::get('/mybranch', function()
{
    return 'mybranch';
});

Route::get('/theirbranch', function()
{
    return 'theirbranch::writiing';
});

/*
 * CRUD
 */
Route::group(['prefix' => 'student','middleware' => 'auth.admin'], function () {
    Route::get('list', 'StudentController@getIndex');    //一覧
    Route::get('new', 'StudentController@new_index');    //入力
    Route::patch('new','StudentController@new_confirm'); //確認
    Route::post('new', 'StudentController@new_finish');  //完了

    Route::get('edit/{id}/', 'StudentController@edit_index');    //編集
    Route::patch('edit/{id}/','StudentController@edit_confirm'); //確認
    Route::post('edit/{id}/', 'StudentController@edit_finish');  //完了

    Route::post('delete/{id}/', 'StudentController@us_delete');  //削除
});

/*
 * uploader demo
 */
Route::group(['middleware' => 'auth.admin'], function() {

    # 入力画面
    Route::get('uploader/', [
        'uses' => 'UploaderController@getIndex',
        'as' => 'uploader.index'
    ]);

    # 確認画面
    Route::patch('uploader/confirm', [
        'uses' => 'UploaderController@confirm',
        'as' => 'uploader.confirm'
    ]);

    # 完了画面
    Route::post('uploader/finish', [
        'uses' => 'UploaderController@finish',
        'as' => 'uploader.finish'
    ]);

});


/*
 * mail demo
 */

Route::group(['middleware' => 'auth.admin'], function() {

    # 入力画面
    Route::get('mail/', [
        'uses' => 'MailDemoController@getIndex',
        'as' => 'mail.index'
    ]);

    # 確認画面
    Route::post('mail/confirm', [
        'uses' => 'MailDemoController@confirm',
        'as' => 'mail.confirm'
    ]);

    # 完了画面
    Route::post('mail/finish', [
        'uses' => 'MailDemoController@finish',
        'as' => 'mail.finish'
    ]);

});


/*
 * insert demo
 */

Route::group(['middleware' => 'auth.admin'], function() {

    # 入力画面
    Route::get('insert/', [
        'uses' => 'InsertDemoController@getIndex',
        'as' => 'insert.index'
    ]);

    # 確認画面
    Route::post('insert/confirm', [
        'uses' => 'InsertDemoController@confirm',
        'as' => 'insert.confirm'
    ]);

    # 完了画面
    Route::post('insert/finish', [
        'uses' => 'InsertDemoController@finish',
        'as' => 'insert.finish'
    ]);

});

# スコープテスト
Route::get('employee/scope_demo','EmployeeController@scopeDemo');

# デバイス判定サービス
Route::get('demo/judge_device','EmployeeController@judge');

# 検索画面＋ページャー
Route::get('employee/list', [
    'uses' => 'EmployeeController@select',
    'as' => 'employee.list'
]);

/*
 * request demo
 */

# 入力画面
Route::get('request/', [
    'uses' => 'RequDemoController@getIndex',
    'as' => 'request.index'
]);

# 確認画面
Route::post('request/confirm', [
    'uses' => 'RequDemoController@confirm',
    'as' => 'request.confirm'
]);


/*
 * validation demo
 */

# 入力画面
Route::get('validation/', [
    'uses' => 'ValiDemoController@getIndex',
    'as' => 'validation.index'
]);

# 確認画面
Route::post('validation/confirm', [
    'uses' => 'ValiDemoController@confirm',
    'as' => 'validation.confirm'
]);

/*
 * Create service
 */

Route::get('bmi/form', [
    'uses' => 'BmiCalController@getIndex',
    'as' => 'bmi.form'
]);

Route::post('bmi/result', [
    'uses' => 'BmiCalController@result',
    'as' => 'bmi.result'
]);

/*
 * Facade test
 */

Route::get('demo/facade',function(){
    return FacadeTest::getMessage();
});

/*
 * Flat UI demo & Bootstrap 実装
 */
Route::get('/flat_ui/{name}','ComponentsController@parts');
Route::get('/bootstrap/{name}','ComponentsController@practice');
Route::get('/users/{name}','ComponentsController@user_list');

/*
 * Echo
 */
Route::get('/testecho/',[
    'uses' => 'TestController@index',
    'as' => 'testecho'
]);

/*
 * 認証自作
 */

//GitHubログイン（ボタンのリンク先）
Route::get('/user/github', [
    'uses' => 'Auth\AuthController@redirectToProvider',
    'as' => 'user.github'
]);

//認証後の戻りURL
Route::get('/user/github/callback', [
    'uses' => 'Auth\AuthController@handleProviderCallback',
    'as' => 'user.github.callback'
]);

//認証後の画面
Route::get('/user/profile', [
    'uses' => 'UserController@getProfile',
    'as' => 'user.profile'
]);


Route::group(['middleware' => 'auth.admin'], function() {

    Route::group(['prefix' => 'user'], function () {

        Route::group(['middleware' => 'guest'], function () {

            Route::get('/signup', [
                'uses' => 'UserController@getSignup',
                'as' => 'user.signup'
            ]);

            Route::post('/signup', [
                'uses' => 'UserController@postSignup',
                'as' => 'user.signup'
            ]);

            Route::get('/signin', [
                'uses' => 'UserController@getSignin',
                'as' => 'user.signin'
            ]);

            Route::post('/signin', [
                'uses' => 'UserController@postSignin',
                'as' => 'user.signin'
            ]);


        });

        Route::group(['middleware' => 'auth'], function () {


            Route::get('/logout', [
                'uses' => 'UserController@getLogout',
                'as' => 'user.logout'
            ]);
        });

    });

});

/*
 * 認証
 */
Route::auth();
//Route::get('/home', 'HomeController@index');
Route::get('/home', function () {
    return view('special.top');
});

/*
 * 特集ページ
 */
Route::get('special/top', function () {
    return view('special.top');
});

Route::get('special/01', 'SpecialController@page01');
Route::get('special/02', 'SpecialController@page02');
Route::get('special/03', 'SpecialController@page03');

/*
 * プルリクエスト動作確認
 */
Route::get('/pullrequ', function()
{
  return 'This file was merged on the bitbucket!';
});

/*
 * デプロイテスト 
 */
Route::get('/deploytest', function()
{
  return 'Deploy test was successful!';
});

/*
 * ブランチテスト
 */
Route::get('/branch', function()
{
  return 'Branch test was successful!<br>commit second<br>commit third';
});

/*
 * カウントダウン
 */
Route::get('count/olympic', 'CountController@olympic');

/*
 * JS練習
 * 2019.01.21
 */

// Vue.js
Route::get('js/vue', 'JsPracticeController@vue');