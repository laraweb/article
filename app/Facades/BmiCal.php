<?php
/**
 * Created by PhpStorm.
 * User: OSAKA-119
 * Date: 2017/09/12
 * Time: 10:55
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class BmiCal extends Facade{
    protected static function getFacadeAccessor()
    {
        return 'bmical';
    }
}
