<?php
/**
 * Created by PhpStorm.
 * User: OSAKA-119
 * Date: 2017/09/01
 * Time: 16:37
 */

namespace app\Services\Messenger;

interface Messagenger{
    public function send($message);
}