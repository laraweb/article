<?php
/**
 * Created by PhpStorm.
 * User: OSAKA-119
 * Date: 2017/09/01
 * Time: 16:41
 */

namespace app\Services\Messenger;

class BikeMessenger implements Messenger{
    public function send($message){
        return "バイク便で $message を届けました。";
    }
}