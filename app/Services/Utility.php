<?php
/**
 * Created by PhpStorm.
 * User: OSAKA-119
 * Date: 2017/09/05
 * Time: 15:01
 */

namespace App\Services;


class Utility
{
    public static function judge_device($ua)
    {
        if ((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) || (strpos($ua, 'Android') !== false)) {
            return "スマホ専用画面";
        } else {
            return "PC専門画面";
        }
    }
}