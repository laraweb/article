<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //timestamps利用しない
    public $timestamps = false;

    protected $fillable = ['dept_id','name','email'];

    //belongsTo設定
    public function dept()
    {
        return $this->belongsTo('App\Dept');
    }

    //scope of address(select kansai)
    public function scopeFindKansai($query)
    {
        return $query->where(function($q){
                $q->Where('address','like','%'.'大阪府'.'%')
                    ->orWhere('address','like','%'.'京都府'.'%')
                    ->orWhere('address','like','%'.'兵庫県'.'%')
                    ->orWhere('address','like','%'.'滋賀県'.'%')
                    ->orWhere('address','like','%'.'奈良県'.'%')
                    ->orWhere('address','like','%'.'和歌山県'.'%');
            });
    }

    //scope of depertment(select develop)
    public function scopeFindDevelop($query)
    {
        return $query->where('dept_id','4');
    }


}
