<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('employees')->delete();

        $faker = Faker\Factory::create('ja_JP');

        $dept = ['1', '2', '3', '4', '5'];

        for ($i = 0; $i < 300; $i++) {
            \DB::table('employees')->insert([
                'dept_id' => $faker->randomElement($dept),
                'name' => $faker->name(),
                'address' => $faker->address(),
                'email' => $faker->email() ,
                'old' => $faker->randomNumber(2),
                'tel' => $faker->phoneNumber()
            ]);
        }
    }
}